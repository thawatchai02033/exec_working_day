// const ConnectionHost = "http://localhost:8080"
// // const ConnectionHost = "https://medhr.medicine.psu.ac.th"

// const ConnectionHost = "/MedHr"
// const ConnectionMedNet = "/MedNet"
// const ConnectionLine = "/Line"
const ConnectionHost = "https://medhr.medicine.psu.ac.th"
const ConnectionMedNet = "https://mednet.psu.ac.th"
// const ConnectionLine = "https://notify-api.line.me"

export default {
    ConnectionHost: ConnectionHost,
    ConnectionMedNet: ConnectionMedNet
}