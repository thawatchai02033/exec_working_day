import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

Vue.config.productionTip = false

Vue.filter('DateToThai', function (value) {
  var DateTrans = "";
  if (value != undefined) {
    var Day = value.split('-')[2];
    var Month = value.split('-')[1];
    var Year = value.split('-')[0];
    var monthNames = ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน',
      'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'
    ];
    DateTrans = Day.split(' ')[0] + " " + monthNames[parseInt(Month) - 1] + " " +
        (parseInt(Year) + 543)
  }
  return (
      DateTrans
  );
})

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
