module.exports = {
  devServer: {
    proxy: {
      '/MedHr': {
        target: 'https://medhr.medicine.psu.ac.th',
        changeOrigin: true,
        secure:false,
        pathRewrite: {'^/MedHr': ''},
        logLevel: 'debug'
      },
      '/MedNet': {
        target: 'https://mednet.psu.ac.th',
        changeOrigin: true,
        secure:false,
        pathRewrite: {'^/MedNet': ''},
        logLevel: 'debug'
      }
    }
  },
  publicPath: './',
  transpileDependencies: [
    "vuetify"
  ],
}
